//
//  ViewController.h
//  TableViewTests
//
//  Created by Rebecca Gutterman on 9/11/13.
//  Copyright (c) 2013 Rebecca Gutterman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
