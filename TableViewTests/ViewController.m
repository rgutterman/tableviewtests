//
//  ViewController.m
//  TableViewTests
//
//  Created by Rebecca Gutterman on 9/11/13.
//  Copyright (c) 2013 Rebecca Gutterman. All rights reserved.
//

#import "ViewController.h"
#import "MyTableViewCell.h"
#import "HeaderCell.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)  NSMutableArray *set1;
@property (strong, nonatomic) NSMutableArray *set2;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        return 2;
}

-(NSMutableArray * )set1{
    if(!_set1){
        _set1=@[@"One",@"Two",@"Three",@"Four",@"Five"].mutableCopy;
    }
    return _set1;
}


-(NSMutableArray * )set2{
    if(!_set2){
        _set2=[[NSMutableArray alloc]init];
    }
    return _set2;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.set1.count;
    }
    else {
        return self.set2.count;
    }
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    MyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MyTableViewCell" ];
    if(indexPath.section==0)
        cell.label.text = [self.set1 objectAtIndex:indexPath.row];
    if(indexPath.section==1)
        cell.label.text = [self.set2 objectAtIndex:indexPath.row];

    return cell;
}
- (IBAction)buttonPressed:(id)sender {
    self.tableView.editing=!self.tableView.editing;
    [self.tableView reloadData];
}

- (IBAction)addPressed:(id)sender {
    [self.set1 addObject:@"New Item"];
    [self.tableView reloadData];
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
               return UITableViewCellEditingStyleDelete;
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 64.0f;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSString * str = [self.set1 objectAtIndex:indexPath.row];
        [self.set1 removeObjectAtIndex:indexPath.row];
        [self.set2 addObject:str];
    }
    double delayInSeconds = .2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.tableView reloadData];
    });
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        HeaderCell * header = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        header.label.text = @"Section 1 Header Row";
        header.backgroundColor=[UIColor yellowColor];
        if (tableView.editing){
            [header.button setTitle:@"Done" forState:UIControlStateNormal];
        }
        else{
            [header.button setTitle:@"Edit" forState:UIControlStateNormal];

        }
        [header setNeedsDisplay];
        return header;
    }
    else
        {
            HeaderCell * header = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
            header.label.text = @"Second Section";
            header.backgroundColor=[UIColor greenColor];
            header.button.hidden=YES;
            [header setNeedsDisplay];
            return header;
        }
    }

@end


