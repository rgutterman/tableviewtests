//
//  main.m
//  TableViewTests
//
//  Created by Rebecca Gutterman on 9/11/13.
//  Copyright (c) 2013 Rebecca Gutterman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
